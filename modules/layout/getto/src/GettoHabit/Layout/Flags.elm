module GettoHabit.Layout.Flags exposing
  ( Model
  )

import Json.Decode as Decode

type alias Model = Decode.Value
