module GettoHabit.Layout.Version exposing
  ( copyright
  , version
  )

copyright = "GETTO systems"
version = "0.1.0"
