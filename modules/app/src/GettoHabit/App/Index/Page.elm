module GettoHabit.App.Index.Page exposing
  ( init
  , subscriptions
  , update
  , view
  )
import GettoHabit.Layout.Flags as Flags
import GettoHabit.Layout.Version as Version

import Html exposing ( Html )

type alias Model = ()

type Msg
  = HelloWorld

init : Flags.Model -> ( Model, Cmd Msg )
init flags = ( (), Cmd.none )

subscriptions : Model -> Sub Msg
subscriptions model = Sub.none

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
  case msg of
    HelloWorld -> ( model, Cmd.none )

view : Model -> Html Msg
view model =
  Version.copyright ++ Version.version
  |> Html.text
