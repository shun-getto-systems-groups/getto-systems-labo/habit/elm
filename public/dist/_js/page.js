"use strict";

try {
  var page = config.page;
  var path = config.path;
  var modules = page.split(".");

  var storageKey = "app";

  if (location.search == "?reset") {
    localStorage.setItem(storageKey, null);
    history.pushState(null,null,"?");
  }

  var version = document.getElementById("version").innerText.replace("version : ", "");

  if (version == "DEV") {
    var reload = document.createElement("script");
    reload.src = "/reload/reload.js";
    document.body.appendChild(reload);
  } else {
    GettoDetect({
      version_to_path: function(version){
        return "/" + version + "/" + path + location.search;
      }
    }).from_current_version(version,function(path) {
      location.href = path;
    });
  }

  //var storage = GettoStorage(storageKey, page);
  //var search = GettoSearch();
  //var dom = GettoDom();

  var module = modules.reduce(function(acc,m){return acc[m];},Elm.GettoHabit.App).EntryPoint;

  var app = module.init({
    node: document.getElementById("elm"),
    flags: {
      //storage: storage.load,
      page: {
        name:   page,
        path:   path,
        //search: search.load,
        loadAt: (new Date()).toISOString(),
        query:  location.pathname + location.search,
      },
      project: {
        name:     document.getElementById("project").innerText,
        company:  document.getElementById("company").innerText,
        title:    document.getElementById("title").innerText,
        subTitle: document.getElementById("sub-title").innerText,
      },
    },
  });

  var subscribe = function(name,func) {
    if (app.ports && app.ports[name]) {
      app.ports[name].subscribe(func);
    }
  };

  var send = function(name,data) {
    if (app.ports && app.ports[name]) {
      app.ports[name].send(data);
    }
  };

  /*
  subscribe("saveGlobal", function(params) {
    storage.saveGlobal.apply(null,params);
  });
  subscribe("savePage", function(params) {
    storage.savePage.apply(null,params);
  });

  subscribe("redirectTo", search.redirectTo);
  subscribe("searchTo",   search.searchTo);

  subscribe("focusTo", function(params) {
    dom.focusTo.apply(null,params);
  });
  */

  subscribe("fixedMidashi", function(_params) {
    setTimeout(function(){
      FixedMidashi.create();
    },300);
  });

  /*
  subscribe("loadYubinBango", function(_params) {
    setTimeout(function(){
      new YubinBango.MicroformatDom();
    },300);
  });

  subscribe("modifyYubinBango", function(_params) {
    setTimeout(function(){
      send("updateYubinBango",
        {
          region:   document.querySelector(".p-region").value,
          locality: document.querySelector(".p-locality").value,
          street:   document.querySelector(".p-street-address").value,
          extended: document.querySelector(".p-extended-address").value,
        }
      )
    },300);
  });
  */
} catch(e) {
  document.getElementById("app").classList.add("display-none");
  document.getElementById("error").classList.remove("display-none");

  if (document.getElementById("version").innerText != "version : DEV") {
    var message = e.name + ": " + e.message;
    var stack = e.stack;

    var host = "api."+location.hostname;

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "//"+host+"/error/notify", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send(
      "message=" + encodeURIComponent(message) +
      "&stack="  + encodeURIComponent(stack)
    );
  }

  throw e;
}
